package com.io.file.serialization;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationDemo {

	public static void main(String[] args) {
		File file = new File("/home/rohit/eclipse-workspace/IOPackage/src/com/io/file/serialization/Emp.ser");
		try {
			FileOutputStream fout = new FileOutputStream(file);
			ObjectOutputStream obj = new ObjectOutputStream(fout);
			
			Employee emp = new Employee(123,"Shaurya",40000,"Faridabad");
			
			obj.writeObject(emp);
			System.out.println("Object is serialized...");
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
